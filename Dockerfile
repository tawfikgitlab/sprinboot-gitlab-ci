FROM openjdk:11
LABEL maintainer="tawfikbouguerba13@gmail.com"
VOLUME /tmp
EXPOSE 8899
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} bookingcar-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","bookingcar-0.0.1-SNAPSHOT.jar"]
